//
//  SpinRecordViewController.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/25/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "AddSpinRecordViewController.h"
#import "SpinRecordStore.h"
#import "SpinRecord.h"
#import <Google/Analytics.h>

@interface AddSpinRecordViewController ()

@property (nonatomic, weak) IBOutlet UITextField *miles;
@property (nonatomic, weak) IBOutlet UITextField *minutes;
@property (nonatomic, weak) IBOutlet UITextField *averageRpm;
@property (nonatomic, weak) IBOutlet UITextField *averageWatts;
@property (nonatomic, weak) IBOutlet UITextField *date;
@property (nonatomic, weak) IBOutlet UITextField *time;

@end

@implementation AddSpinRecordViewController

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSString *screen = NSStringFromClass ([self class]);
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screen];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(saveRide:)];
    rightButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.date setInputView:datePicker];
    self.date.tintColor = [UIColor clearColor];

    // disable copy/paste
    [datePicker addTarget:self action:@selector(dateChanged:)
     forControlEvents:UIControlEventValueChanged];
    
    UIDatePicker *timePicker = [[UIDatePicker alloc] init];
    timePicker.datePickerMode = UIDatePickerModeTime;
    [self.time setInputView:timePicker];
    self.time.tintColor = [UIColor clearColor];

    // disable copy/paste
    [timePicker addTarget:self action:@selector(timeChanged:)
         forControlEvents:UIControlEventValueChanged];
    
    [self setDateLabel];
    [self setTimeLabel];
    
    [self.miles becomeFirstResponder];
}

- (IBAction)requiredFieldChanged {
    
    BOOL enableAddButton = (self.miles.text.length > 0) && (self.minutes.text.length > 0);
    self.navigationItem.rightBarButtonItem.enabled = enableAddButton;
}

- (IBAction)saveRide:(id)sender {
    
    NSString *minutes = self.minutes.text;
    NSString *miles = self.miles.text;
    NSString *avgRPM = self.averageRpm.text;
    NSString *avgWatts = self.averageWatts.text;
    
    self.spinRecord.minutes = [minutes integerValue];
    self.spinRecord.miles = [miles doubleValue];
    self.spinRecord.avgRpm = [avgRPM integerValue];
    self.spinRecord.avgWatts = [avgWatts integerValue];
        
    [[SpinRecordStore sharedStore] addSpinRecord:self.spinRecord];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell.contentView.subviews[1] becomeFirstResponder];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)dateChanged:(id)sender {
    
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    self.spinRecord.date = datePicker.date;
    [self setDateLabel];
}

-(void)timeChanged:(id)sender {

    UIDatePicker *timePicker = (UIDatePicker *)sender;
    self.spinRecord.time = timePicker.date;
    [self setTimeLabel];
}

-(void)setDateLabel {
    
    self.date.text = [NSDateFormatter localizedStringFromDate:self.spinRecord.date
                                                    dateStyle:NSDateFormatterMediumStyle
                                                    timeStyle:NSDateFormatterNoStyle];
}

-(void)setTimeLabel {

    self.time.text = [NSDateFormatter localizedStringFromDate:self.spinRecord.time
                                                    dateStyle:NSDateFormatterNoStyle
                                                    timeStyle:NSDateFormatterShortStyle];
}

@end
