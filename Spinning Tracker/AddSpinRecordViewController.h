//
//  SpinRecordViewController.h
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/25/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

@class SpinRecord;
#import <UIKit/UIKit.h>

@interface AddSpinRecordViewController : UITableViewController

@property (nonatomic, strong) SpinRecord *spinRecord;

@end
