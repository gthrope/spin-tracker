//
//  SpinRecord.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecord.h"

@implementation SpinRecord

-(instancetype)init {
    
    self = [super init];
    if(self) {
        
        self.date = [[NSDate alloc] init];
        self.time = [[NSDate alloc] init];
    }
    
    return self;
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if(self) {
        self.miles = [aDecoder decodeDoubleForKey:@"miles"];
        self.minutes = [aDecoder decodeIntegerForKey:@"minutes"];
        self.avgRpm = [aDecoder decodeIntegerForKey:@"avgRpm"];
        self.avgWatts = [aDecoder decodeIntegerForKey:@"avgWatts"];
        self.date = [aDecoder decodeObjectForKey:@"date"];
        self.time = [aDecoder decodeObjectForKey:@"time"];
    }
    
    return self;
}

- (NSString *)description {
    
    NSString *date = [NSDateFormatter localizedStringFromDate:self.date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
    return [[NSString alloc] initWithFormat:@"%.1f miles on %@", self.miles, date];
}

- (void)dealloc {

    NSLog(@"Destroyed: %@", self);
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeDouble:self.miles forKey:@"miles"];
    [aCoder encodeInteger:self.minutes forKey:@"minutes"];
    [aCoder encodeInteger:self.avgRpm forKey:@"avgRpm"];
    [aCoder encodeInteger:self.avgWatts forKey:@"avgWatts"];
    [aCoder encodeObject:self.date forKey:@"date"];
    [aCoder encodeObject:self.time forKey:@"time"];
}

@end
