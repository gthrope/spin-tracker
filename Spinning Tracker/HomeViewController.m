//
//  HomeViewController.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 3/7/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecord.h"
#import "SpinRecordStore.h"
#import "JBBarChartView.h"
#import "AddSpinRecordViewController.h"
#import "HomeViewController.h"
#import <Google/Analytics.h>

@interface HomeViewController () <JBBarChartViewDataSource, JBBarChartViewDelegate>

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, weak) IBOutlet JBBarChartView *barChart;
@property (nonatomic, weak) IBOutlet UILabel *minLabel;
@property (nonatomic, weak) IBOutlet UILabel *maxLabel;

@property (nonatomic, weak) IBOutlet UILabel *averageRide;
@property (nonatomic, weak) IBOutlet UILabel *totals;
@property (nonatomic, weak) IBOutlet UILabel *averageRPM;
@property (nonatomic, weak) IBOutlet UILabel *averageWatts;

@end

@implementation HomeViewController

-(void)viewDidLoad {
    
    [super viewDidLoad];
        
    self.barChart.delegate = self;
    self.barChart.dataSource = self;
    self.barChart.minimumValue = 0.0;
    self.barChart.maximumValue = 1.0;
    
    [self.segmentControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    SpinRecordStore *sharedStore = [SpinRecordStore sharedStore];
    
    NSString *rideString = ([sharedStore totalRides] == 1) ? @"ride" : @"rides";
    
    self.averageRide.text = [NSString stringWithFormat:@"%.1f miles in %.1f minutes", [sharedStore overallAverageMiles], [sharedStore overallAverageMinutes]];
    self.totals.text = [NSString stringWithFormat:@"%.1f miles in %lu %@", [sharedStore totalMiles],(unsigned long)[sharedStore totalRides], rideString];
    self.averageRPM.text = [NSString stringWithFormat:@"%.1f", [sharedStore overallAverageRpm]];
    self.averageWatts.text = [NSString stringWithFormat:@"%.1f", [sharedStore overallAverageWatts]];

    [self loadGraph];
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    
    NSString *screen = NSStringFromClass ([self class]);
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screen];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

// user selects different graph
-(void)segmentValueChanged:(UISegmentedControl *)segment {
    
    [self loadGraph];
}

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView {

    return 7;
}

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    NSArray *allRides = [[SpinRecordStore sharedStore] allRecords];
    NSUInteger numRides = allRides.count;
    
    CGFloat value = 0.0;
    
    if(index < numRides) {
        
        NSUInteger rideIndex = numRides - MIN(numRides, 7) + index;
        SpinRecord *spinRecord = [allRides objectAtIndex:rideIndex];
        
        switch (self.segmentControl.selectedSegmentIndex) {
            case 0:
                value = spinRecord.miles;
                break;
            case 1:
                value = spinRecord.avgRpm;
                break;
            case 2:
                value = spinRecord.avgWatts;
                break;
        }
    }
    
    return value;
}

-(void)loadGraph {

    [self.barChart reloadData];
    
    NSString *minLabelText = nil;
    NSString *maxLabelText = nil;
    
    NSArray *allRides = [[SpinRecordStore sharedStore] allRecords];
    if(allRides.count > 0) {
        
        minLabelText = [NSString stringWithFormat:@"%.0f", self.barChart.minimumValue];
        maxLabelText = [NSString stringWithFormat:@"%.0f", self.barChart.maximumValue];
    }
    
    self.minLabel.text = minLabelText;
    self.maxLabel.text = maxLabelText;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"addRideSegue"]) {
        AddSpinRecordViewController *addSpinRecord = segue.destinationViewController;
        addSpinRecord.spinRecord = [[SpinRecord alloc] init];
    }
}

@end
