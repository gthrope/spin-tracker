//
//  SpinRecordStore.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecordStore.h"
#import "SpinRecord.h"

@interface SpinRecordStore ()

@property (nonatomic, strong) NSMutableArray *privateRecords;

@end

@implementation SpinRecordStore

#pragma mark - Init methods

- (instancetype)initPrivate {

    self = [super init];
    if(self) {
        
        NSString *path = [self itemArchivePath];
        self.privateRecords = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        
        if(!self.privateRecords) {
            self.privateRecords = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[SpinRecordStore sharedStore]"
                                 userInfo:nil];
    return nil;
}

#pragma mark - Archiving methods

-(NSString *)itemArchivePath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                       NSUserDomainMask, YES);
    
    NSString *documentDirectory = [documentDirectories firstObject];
    
    return [documentDirectory stringByAppendingPathComponent:@"items.archive"];
}

-(BOOL)saveChanges {
    
    NSString *path = [self itemArchivePath];
    return [NSKeyedArchiver archiveRootObject:self.privateRecords toFile:path];
}

#pragma mark - Basic store manipulation

+ (instancetype)sharedStore {
    
    static SpinRecordStore *sharedStore;
    
    if(!sharedStore) {
        sharedStore = [[SpinRecordStore alloc] initPrivate];
    }
    
    return sharedStore;
}

- (NSArray *)allRecords {
    
    return [self.privateRecords copy];
}

- (void)addSpinRecord:(SpinRecord *)spinRecord {
    
    [self.privateRecords addObject:spinRecord];
}

- (void)removeItem:(SpinRecord *)item {
    
    [self.privateRecords removeObjectIdenticalTo:item];
}

#pragma mark - Meta-stats

-(CGFloat) totalMiles {

    CGFloat totalMiles = 0.0;
    for(SpinRecord *record in self.privateRecords) {
        totalMiles += record.miles;
    }
    return totalMiles;
}

-(NSUInteger)totalMinutes {

    NSUInteger totalMinutes = 0;
    for(SpinRecord *record in self.privateRecords) {
        totalMinutes += record.minutes;
    }
    return totalMinutes;
}

-(CGFloat)overallAverageMiles {
    
    CGFloat totalMiles = self.totalMiles;
    NSUInteger totalRides = self.totalRides;
    
    CGFloat averageMiles = 0.0;
    
    if(totalRides > 0) {
        averageMiles = totalMiles / totalRides;
    }
    
    return averageMiles;
}

-(CGFloat)overallAverageMinutes {
    
    CGFloat totalMinutes = self.totalMinutes;
    NSUInteger totalRides = self.totalRides;
    
    CGFloat averageMinutes = 0.0;
    
    if(totalRides > 0) {
        averageMinutes = totalMinutes / totalRides;
    }
    
    return averageMinutes;
}


-(CGFloat)overallAverageRpm {
    
    CGFloat totalRpmMinutes = 0.0;
    CGFloat totalMinutes = 0.0;
    CGFloat overallAverageRpm = 0.0;
    
    for(SpinRecord *record in self.privateRecords) {
        if(record.avgRpm > 0) {
            totalRpmMinutes += record.avgRpm * record.minutes;
            totalMinutes += record.minutes;
        }
    }
    
    if(totalMinutes > 0) {
        overallAverageRpm = totalRpmMinutes / totalMinutes;
    }
    
    return overallAverageRpm;
}

-(CGFloat)overallAverageWatts {
    
    CGFloat totalWattMinutes = 0.0;
    CGFloat totalMinutes = 0.0;
    CGFloat overallAverageWatts = 0.0;
    
    for(SpinRecord *record in self.privateRecords) {
        if(record.avgWatts > 0) {
            totalWattMinutes += record.avgWatts * record.minutes;
            totalMinutes += record.minutes;
        }
    }
    
    if(totalMinutes > 0) {
        overallAverageWatts = totalWattMinutes / totalMinutes;
    }
    
    return overallAverageWatts;
}

-(NSUInteger)totalRides {
    
    return [self.privateRecords count];
}

@end
