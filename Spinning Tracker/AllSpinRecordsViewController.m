//
//  HomeViewController.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecord.h"
#import "SpinRecordStore.h"
#import "AllSpinRecordsViewController.h"
#import "RideDetailViewController.h"
#import <Google/Analytics.h>

@implementation AllSpinRecordsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:@"UITableViewCell"];

    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setEditButtonState];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    
    NSString *screen = NSStringFromClass ([self class]);
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screen];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];

    SpinRecord *record = [[[SpinRecordStore sharedStore] allRecords] objectAtIndex:indexPath.row];
    cell.textLabel.text = [record description];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[SpinRecordStore sharedStore] allRecords] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // yes, this is a hack
    [self performSegueWithIdentifier:@"rideDetailSegue" sender:indexPath];
}

- (void)   tableView:(UITableView *)tableView
  commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
   forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If the table view is asking to commit a delete command...
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray *allRecords = [[SpinRecordStore sharedStore] allRecords];
        SpinRecord *spinRecord = allRecords[indexPath.row];
        [[SpinRecordStore sharedStore] removeItem:spinRecord];
        
        // Also remove that row from the table view with an animation
        [tableView deleteRowsAtIndexPaths:@[indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if(editing == NO) {

        [self setEditButtonState];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:@"rideDetailSegue"]) {

        // yes, this is a hack
        NSIndexPath *indexPath = sender;
        SpinRecord *spinRecord = [[[SpinRecordStore sharedStore] allRecords] objectAtIndex:indexPath.row];
        
        RideDetailViewController *rideDetail = segue.destinationViewController;
        rideDetail.spinRecord = spinRecord;
    }
}

-(void)setEditButtonState {
    
    NSArray *allRecords = [[SpinRecordStore sharedStore] allRecords];
    if(allRecords.count == 0) {
        self.editButtonItem.enabled = NO;
    }
}

@end
