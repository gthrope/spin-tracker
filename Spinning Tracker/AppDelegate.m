//
//  AppDelegate.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecordStore.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    return YES;
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
    
    BOOL success = [[SpinRecordStore sharedStore] saveChanges];
    if(success) {
        NSLog(@"Saved all of the SpinRecords");
    } else {
       NSLog(@"Could not save any of the SpinRecords");
    }
}

@end
