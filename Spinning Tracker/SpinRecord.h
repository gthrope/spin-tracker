//
//  SpinRecord.h
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinRecord : NSObject <NSCoding>

@property (nonatomic) CGFloat miles;
@property (nonatomic) NSUInteger minutes;
@property (nonatomic) NSUInteger avgRpm;
@property (nonatomic) NSUInteger avgWatts;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDate *time;

@end
