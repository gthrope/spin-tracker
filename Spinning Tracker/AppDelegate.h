//
//  AppDelegate.h
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

