//
//  RideDetailViewController.m
//  Spinning Tracker
//
//  Created by Glenn Thrope on 3/7/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "SpinRecord.h"
#import "RideDetailViewController.h"
#import <Google/Analytics.h>

@interface RideDetailViewController ()

@property (nonatomic, weak) IBOutlet UILabel *miles;
@property (nonatomic, weak) IBOutlet UILabel *minutes;
@property (nonatomic, weak) IBOutlet UILabel *avgRpm;
@property (nonatomic, weak) IBOutlet UILabel *avgWatts;
@property (nonatomic, weak) IBOutlet UILabel *date;
@property (nonatomic, weak) IBOutlet UILabel *time;

@end

@implementation RideDetailViewController

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    SpinRecord *spinRecord = self.spinRecord;
    
    self.miles.text = [NSString stringWithFormat:@"%.1f", spinRecord.miles];
    self.minutes.text = [NSString stringWithFormat:@"%lu", (unsigned long)spinRecord.minutes];
    self.avgRpm.text = [NSString stringWithFormat:@"%lu", (unsigned long)spinRecord.avgRpm];
    self.avgWatts.text = [NSString stringWithFormat:@"%lu", (unsigned long)spinRecord.avgWatts];
    self.date.text = [NSDateFormatter localizedStringFromDate:spinRecord.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
    self.time.text = [NSDateFormatter localizedStringFromDate:spinRecord.time dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
    
    NSString *screen = NSStringFromClass ([self class]);
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screen];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


@end
