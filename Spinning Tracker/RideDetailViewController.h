//
//  RideDetailViewController.h
//  Spinning Tracker
//
//  Created by Glenn Thrope on 3/7/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SpinRecord;

@interface RideDetailViewController : UITableViewController

@property (nonatomic, strong) SpinRecord *spinRecord;

@end
