//
//  SpinRecordStore.h
//  Spinning Tracker
//
//  Created by Glenn Thrope on 2/23/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SpinRecord;

@interface SpinRecordStore : NSObject

@property (nonatomic, readonly, copy) NSArray *allRecords;
@property (nonatomic, readonly) CGFloat totalMiles;
@property (nonatomic, readonly) NSUInteger totalMinutes;
@property (nonatomic, readonly) CGFloat overallAverageMiles;
@property (nonatomic, readonly) CGFloat overallAverageMinutes;
@property (nonatomic, readonly) CGFloat overallAverageRpm;
@property (nonatomic, readonly) CGFloat overallAverageWatts;
@property (nonatomic, readonly) NSUInteger totalRides;

+ (instancetype)sharedStore;
- (void)addSpinRecord:(SpinRecord *)spinRecord;
- (void)removeItem:(SpinRecord *)item;
- (BOOL)saveChanges;

@end
